namespace EFDemo.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateBookmarks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookmarks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Uri = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            DropTable("dbo.Bookmarks");
        }

    }
}
