namespace EFDemo.Model.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EFDemo.Model.DemoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "EFDemo.Model.DemoContext";
        }

        protected override void Seed(EFDemo.Model.DemoContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Bookmarks.AddOrUpdate(
                new Bookmark() { Id=1, Title=".Net Blog", Uri= "https://blogs.msdn.microsoft.com/dotnet/" }
            );
        }
    }
}
