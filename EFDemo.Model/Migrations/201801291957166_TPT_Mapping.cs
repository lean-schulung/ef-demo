namespace EFDemo.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TPT_Mapping : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Company = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.People", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Trainers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Profile = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.People", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trainers", "Id", "dbo.People");
            DropForeignKey("dbo.Students", "Id", "dbo.People");
            DropIndex("dbo.Trainers", new[] { "Id" });
            DropIndex("dbo.Students", new[] { "Id" });
            DropTable("dbo.Trainers");
            DropTable("dbo.Students");
            DropTable("dbo.People");
        }
    }
}
