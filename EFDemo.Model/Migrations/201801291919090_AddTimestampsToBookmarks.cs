namespace EFDemo.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimestampsToBookmarks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bookmarks", "CreatedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Bookmarks", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bookmarks", "RowVersion");
            DropColumn("dbo.Bookmarks", "CreatedAt");
        }
    }
}
