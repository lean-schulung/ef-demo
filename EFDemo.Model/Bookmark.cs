﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Model
{
    public class Bookmark
    {
        public Bookmark()
        {
            CreatedAt = DateTime.Now;
        }

        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        public string Title { get; set; }

        [Required]
        public string Uri { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Timestamp]

        public byte[] RowVersion { get; set; }
    }
}
