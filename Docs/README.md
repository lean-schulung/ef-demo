# EFDemo Solution

Alle Schritte zum Thema Nuget-Paket bzw. Migrationen werden in der Paket-Manager-Console
ausgef�hrt.

## Project: EFDemo.Model
> DB Model using Entity Framework Code Only

1. Neues Projekt, Typ: .Net Framework Klassenbibliothek
2. Class1 l�schen
3. Install Entity Framework: ```Install-Package EntityFramework```
4. Entity-Klasse ```Bookmark``` anlegen
5. Context-Klasse ```DemoContext``` anlegen

## Project: EFDemo.ConsoleClient

1. Neues Projekt, Typ: .Net Framework Console App
2. Entity Framework installieren, Referenz auf Model Library hinzuf�gen
3. Main-Methode implementieren
4. Client starten

Ergebnis: je nach Installation wird eine neue DB erzeugt - entweder in einer
SqlExpress-Instanz oder in der LocalDB-Instanz. Als Alternative kann ein
Connection-String in der app.config konfiguriert werden und im Constructor der
DemoContext-Klasse gesetzt werden.

## Project: EFDemo.Model

1. Migrationen einschalten: ```Enable-Migrations```
2. Seed-Methode in der Configuration-Klasse �berschreiben
3. Datenbank f�llen: Update-Database
4. Neue Properties in der Bookmarks-Klasse: CreatedAt und RowVersion
5. Migration erzeugen: ```Add-Migration AddTimestampsToBookmarks```
6. Datenbank updaten
7. Im Main Code zum �ndern des Bookmarks schreiben und Log checken.

## Project: EFDemo.Model

1. Demo f�r TPT (Table-Per-Type) Mapping: Klassen Person, Trainer, Student
2. Im Kontext lediglich ein Set f�r die Person hinzuf�gen
3. Erzeuge Testweise eine Migration
4. Mappe die Subklassen (entweder per Table-Attribut oder Fluent-Api)
