﻿using EFDemo.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Trainer t1;

            using (var ctx = new DemoContext())
            {
                ctx.Database.Log = Console.Write;

                var bm = ctx.Bookmarks.FirstOrDefault();
                bm.Title = ".Net Developer Blog";

                try
                {
                    ctx.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    var entry = ex.Entries.First();

                    var myCurrentObject = (Bookmark)entry.Entity;

                    var dbValues = entry.GetDatabaseValues();
                    var dbObject = (Bookmark)dbValues.ToObject();

                    // Decide who wins
                    // 1. Me - Last in Wins
                    myCurrentObject.RowVersion = dbObject.RowVersion;
                    ctx.SaveChanges();

                    // 2. DB - First to win
                    // entry.Reload();
                }

                t1 = new Trainer() { FirstName = "Micha", LastName = "Alt", Profile = "JavaScript Frontend and .Net Backend" };
                var s1 = new Student() { FirstName = "Jens", LastName = "Weiß-Nicht", Company = "FastLane" };

                ctx.Persons.Add(t1);
                ctx.Persons.Add(s1);
                ctx.SaveChanges();

                var students = ctx.Persons.OfType<Student>().ToList();
            }

            using (var ctx = new DemoContext())
            {
                ctx.Database.Log = Console.Write;

                t1.FirstName = "Michael";

                var toChange = ctx.Persons.Find(t1.Id);
                ctx.Entry(toChange).CurrentValues.SetValues(t1);
                ctx.SaveChanges();
            }


            using (var ctx = new DemoContext())
            {
                foreach (var item in ctx.Persons)
                {
                    ctx.Persons.Remove(item);
                }
                ctx.SaveChanges();
            }
        }
    }
}
